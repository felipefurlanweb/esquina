﻿(function () {
    'use strict';
    angular
        .module('app')
        .directive('validDate', sisValidDate);

    sisValidDate.$inject = ['$locale'];
    function sisValidDate($locale) {
        // http://www.benlesh.com/2012/12/angular-js-custom-validation-via.html

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function ($scope, $element, $attr, $ctrl) {
                $ctrl.$parsers = [];
                $ctrl.$formatters = [];
                $ctrl.$parsers.unshift(function (value) {

                    //console.log($locale.DATETIME_FORMATS);
                    //console.log('parsers', value);
                    
                    var mask = $locale.DATETIME_FORMATS.customDate.toUpperCase();
                    var date = moment(value, mask);
                  
                    $ctrl.$setValidity('validDate', date.isValid());
                    
                    // if it's valid, return the value to the model, 
                    // otherwise return undefined.
                    return (date.isValid()) ? date.format() : undefined;
                });

                $ctrl.$formatters.unshift(function (value) {

                    //console.log($locale.DATETIME_FORMATS);
                    //console.log('formatters', value);
                    
                    if (value === undefined || value === null) {
                        $ctrl.$setValidity('validDate', true)
                        return undefined;
                    }

                    var mask = $locale.DATETIME_FORMATS.customDate.toUpperCase();
                    var date = moment(value);

                    // validate.
                    $ctrl.$setValidity('validDate', date.isValid());

                    // return the value or nothing will be written to the DOM.
                    return date.format(mask);
                });

                // EVENTS

                $element.bind('blur', function () {
                    if (!$ctrl.$valid) {
                        $element.val(null);
                    }
                });

            }
        };
    }

}());