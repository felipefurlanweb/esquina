﻿(function () {
    'use strict';
    angular
        .module('app')
        .directive('validTime', validTime);

    validTime.$inject = [];
    function validTime() {
        var directive = {
            restrict: 'A',
            require: 'ngModel',
            // http://www.benlesh.com/2012/12/angular-js-custom-validation-via.html
            link: function (scope, element, attrs, ctrl) {
                var format = "YYYY-MM-DD h:mm";
                var startDate = "2000-01-01 ";

                ctrl.$parsers.unshift(function (value) {
                    //console.log('PARSING', timeString, isValid);

                    // YYYY-MM-DD(T)HH:MM
                    var timeString = startDate + value;
                    var isValid = moment(timeString, format).isValid();

                    ctrl.$setValidity('validTime', isValid);
                    return isValid? value : undefined;
                });

                ctrl.$formatters.unshift(function (value) {
                    //console.log('FORMATTING', value);

                    // YYYY-MM-DD(T)HH:MM
                    var timeString = startDate + value;
                    var isValid = moment(timeString, format).isValid();

                    ctrl.$setValidity('validTime', isValid);
                    return value;
                })
            }
        };

        return directive;
    }
}());