﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardPagamentoDebitoController', ComprarEcardPagamentoDebitoController)

    ComprarEcardPagamentoDebitoController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardPagamentoDebitoController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;
        $scope.goCompraFinalizadaDebito = goCompraFinalizadaDebito;
        $scope.goComprarEcardPagamento = goComprarEcardPagamento;

        function goComprarEcardPagamento(){
            $state.go("comprarEcardPagamento");
        }

        function goCompraFinalizadaDebito(){
            $state.go("comprarEcardPagamentoDebitoFinalizada");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());