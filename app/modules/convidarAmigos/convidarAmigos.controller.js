﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ConvidarAmigosController', ConvidarAmigosController)

    ConvidarAmigosController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ConvidarAmigosController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;

        function goHome(){
            $state.go("home");
        }

    }

}());