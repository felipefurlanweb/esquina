﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('LoginController', LoginController)

    LoginController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function LoginController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        // $ionicPlatform.registerBackButtonAction(function(e) {
        //   showBoxHome();
        // }, 100);

        $scope.boxEsqueciSenha = false;
        $scope.boxHome = true;

        $scope.goHome = goHome;
        $scope.goCadastro1 = goCadastro1;
        
        $scope.showBoxHome = showBoxHome;
        $scope.showBoxEsqueciSenha = showBoxEsqueciSenha;

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        function goCadastro1(){
            $state.go("cadastro1");
        }

        function showBoxHome(){
            $scope.boxEsqueciSenha = false;
            $scope.boxHome = true;
        }

        function showBoxEsqueciSenha(){
            $scope.boxHome = false;
            $scope.boxEsqueciSenha = true;
        }


        function goHome(){
            $state.go("home");
        }







    }

}());