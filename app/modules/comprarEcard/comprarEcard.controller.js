﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardController', ComprarEcardController)

    ComprarEcardController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;
        $scope.goCadastro = goCadastro;
        $scope.goComprarEcardDados = goComprarEcardDados;
        
        function goComprarEcardDados(){
            $state.go("comprarEcardDados");
        }
        
        function goCadastro(){
            $state.go("cadastro");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());