﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardEnderecoController', ComprarEcardEnderecoController)

    ComprarEcardEnderecoController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardEnderecoController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;
        $scope.goComprarEcardPagamento = goComprarEcardPagamento;
        $scope.goComprarEcardDados = goComprarEcardDados;
        
        function goComprarEcardDados(){
            $state.go("comprarEcardDados");
        }
        
        function goComprarEcardPagamento(){
            $state.go("comprarEcardPagamento");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());