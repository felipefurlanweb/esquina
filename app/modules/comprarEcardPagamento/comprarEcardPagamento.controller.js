﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardPagamentoController', ComprarEcardPagamentoController)

    ComprarEcardPagamentoController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardPagamentoController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        // $ionicPlatform.registerBackButtonAction(function(e) {
        //   showBoxHome();
        // }, 100);

        $scope.goHome = goHome;
        $scope.goComprarEcardPagamentoCredito = goComprarEcardPagamentoCredito;
        $scope.goComprarEcardEndereco = goComprarEcardEndereco;
        $scope.goComprarEcardPagamentoDebito = goComprarEcardPagamentoDebito;

        function goComprarEcardPagamentoDebito(){
            $state.go("comprarEcardPagamentoDebito");
        }

        function goComprarEcardEndereco(){
            $state.go("comprarEcardEndereco");
        }
        
        function goComprarEcardPagamentoCredito(){
            $state.go("comprarEcardPagamentoCredito");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());