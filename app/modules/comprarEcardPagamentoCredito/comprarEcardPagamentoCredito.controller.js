﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardPagamentoCreditoController', ComprarEcardPagamentoCreditoController)

    ComprarEcardPagamentoCreditoController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardPagamentoCreditoController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.parcelas = false;
        $scope.goHome = goHome;
        $scope.goCompraFinalizadaCredito = goCompraFinalizadaCredito;
        $scope.goComprarEcardPagamento = goComprarEcardPagamento;
        $scope.showParcelas = showParcelas;
        $scope.closeParcelas = closeParcelas;

        function showParcelas(){
            $scope.parcelas = true;
            $('html, body').animate({
                scrollTop: "0px"
            }, 50);
        }

        function closeParcelas(){
            $scope.parcelas = false;
        }

        function goComprarEcardPagamento(){
            $state.go("comprarEcardPagamento");
        }

        function goCompraFinalizadaCredito(){
            $state.go("comprarEcardPagamentoCreditoFinalizada");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());