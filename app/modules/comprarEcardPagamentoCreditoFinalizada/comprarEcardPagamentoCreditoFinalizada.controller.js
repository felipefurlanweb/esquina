﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardPagamentoCreditoFinalizadaController', ComprarEcardPagamentoCreditoFinalizadaController)

    ComprarEcardPagamentoCreditoFinalizadaController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardPagamentoCreditoFinalizadaController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;
        $scope.goComprarEcardPagamentoCredito = goComprarEcardPagamentoCredito;

        function goComprarEcardPagamentoCredito(){
            $state.go("comprarEcardPagamentoCredito");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());