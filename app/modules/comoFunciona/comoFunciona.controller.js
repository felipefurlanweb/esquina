﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComoFuncionaController', ComoFuncionaController)

    ComoFuncionaController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComoFuncionaController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;


	    // $ionicPlatform.registerBackButtonAction(function(e) {
	    //   showBoxHome();
	    // }, 100);

        function goHome(){
            $state.go("home");
        }

    }

}());