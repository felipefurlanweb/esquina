﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardDadosController', ComprarEcardDadosController)

    ComprarEcardDadosController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardDadosController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;
        $scope.goComprarEcardEndereco = goComprarEcardEndereco;
        $scope.goComprarEcard = goComprarEcard;

        // $ionicPlatform.registerBackButtonAction(function(e) {
        //   showBoxHome();
        // }, 100);

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        function goComprarEcard(){
            $state.go("comprarEcard");
        }
        
        function goComprarEcardEndereco(){
            $state.go("comprarEcardEndereco");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());