﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('QuantoSalveiController', QuantoSalveiController)

    QuantoSalveiController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function QuantoSalveiController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;

        function goHome(){
            $state.go("home");
        }

    }

}());