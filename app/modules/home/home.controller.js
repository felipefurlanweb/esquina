﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('HomeController', HomeController)

    HomeController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function HomeController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.boxHome = true;
        $scope.boxAllCategorias = false;
        $scope.menu = false;

        $scope.goHome = goHome;
        $scope.showMenu = showMenu;
        $scope.closeMenu = closeMenu;
        $scope.openAllCategorias = openAllCategorias;
        $scope.goComoFunciona = goComoFunciona;
        $scope.goFaq = goFaq;
        $scope.goComprarEcard = goComprarEcard;
        $scope.goConvidarAmigos = goConvidarAmigos;
        $scope.goQuantoSalvei = goQuantoSalvei;

	    // $ionicPlatform.registerBackButtonAction(function(e) {
	    //   showBoxHome();
	    // }, 100);

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        function goQuantoSalvei(){
            $state.go("quantoSalvei");
        }

        function goConvidarAmigos(){
            $state.go("convidarAmigos");
        }

        function goComprarEcard(){
            $state.go("comprarEcard");
        }

        function goFaq(){
            $state.go("faq");
        }

        function goComoFunciona(){
            $state.go("comoFunciona");
        }

        function goHome(){
            $scope.menu = false;
            $scope.boxAllCategorias = false;
            $scope.boxHome = true;
        }

        function openAllCategorias(){
            $scope.menu = false;
            $scope.boxHome = false;
            $scope.boxAllCategorias = true;
        }

        function showMenu(){
            $scope.menu = true;
        }

        function closeMenu(){
            $scope.menu = false;
        }
    
    }

}());