(function () {
    'use strict';
    angular
        .module('app')
        .controller('CadastroController', CadastroController)

    CadastroController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function CadastroController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        // $ionicPlatform.registerBackButtonAction(function(e) {
        //   showBoxHome();
        // }, 100);

        $scope.goLogin = goLogin;
        $scope.goCadastro1 = goCadastro1;
        $scope.goCadastroConfirmaEmail = goCadastroConfirmaEmail;

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        function goCadastro1(){
            $state.go("cadastro1");
        }

        function goCadastroConfirmaEmail(){
            $state.go("cadastroConfirmaEmail");
        }

        function goLogin(){
            $state.go("login");
        }

    }

}());