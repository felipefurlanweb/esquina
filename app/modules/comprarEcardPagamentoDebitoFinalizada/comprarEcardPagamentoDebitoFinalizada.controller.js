﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('ComprarEcardPagamentoDebitoFinalizadaController', ComprarEcardPagamentoDebitoFinalizadaController)

    ComprarEcardPagamentoDebitoFinalizadaController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function ComprarEcardPagamentoDebitoFinalizadaController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        $scope.goHome = goHome;
        $scope.goComprarEcardPagamentoDebito = goComprarEcardPagamentoDebito;

        function goComprarEcardPagamentoDebito(){
            $state.go("comprarEcardPagamentoDebito");
        }

        function goHome(){
            $state.go("home");
        }

    }

}());