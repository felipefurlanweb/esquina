(function () {
    'use strict';
    angular
        .module('app')
        .controller('Cadastro1Controller', Cadastro1Controller)

    Cadastro1Controller.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function Cadastro1Controller($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        // $ionicPlatform.registerBackButtonAction(function(e) {
        //   showBoxHome();
        // }, 100);

        $scope.goHome = goHome;
        $scope.goCadastro = goCadastro;
        $scope.goLogin = goLogin;
        $scope.goComprarEcard = goComprarEcard;

        ///////////////////////////////////////////////////////////////////////////////////////////////////


        function goComprarEcard(){
            $state.go("comprarEcard");
        }

        function goCadastro(){
            $state.go("cadastro");
        }

        function goHome(){
            $state.go("goHome");
        }

        function goLogin(){
            $state.go("login");
        }

        function goLogin(){
            $state.go("home");
        }

    }

}());