(function () {
    'use strict';
    angular
        .module('app')
        .controller('CadastroConfirmaEmailController', CadastroConfirmaEmailController)

    CadastroConfirmaEmailController.$inject = ['$rootScope', '$scope', '$filter', 'SettingsHelper', 
    'localStorageService', '$state', '$ionicPlatform', '$ionicHistory'];
    function CadastroConfirmaEmailController($rootScope, $scope, $filter, SettingsHelper, 
        localStorageService, $state, $ionicPlatform, $ionicHistory) {

        // $ionicPlatform.registerBackButtonAction(function(e) {
        //   showBoxHome();
        // }, 100);

        $scope.goLogin = goLogin;
        $scope.goHome = goHome;

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        function goHome(){
            $state.go("goHome");
        }

        function goLogin(){
            $state.go("login");
        }

    }

}());