﻿(function () {
	'use strict';

	angular
		.module("app")
		.run(run);

	run.$inject = ['$rootScope','$state', '$q'];
	function run($rootScope, $state, $q) {

		$rootScope.$on("$stateChangeError", function (event, current, previous, eventObj) {
		});

		$rootScope.loader = false;
		$rootScope.plantao = null;

	}


}());
