﻿(function () {
    'use strict';
    angular
        .module('app')
        .factory('SettingsHelper', SettingsHelper);

    SettingsHelper.$inject = [];
    function SettingsHelper() {
        var service = {
            BaseUrl: 'http://localhost/plandoc/',
            //BaseUrl: 'http://ffdevweb.com.br/projetos/plandoc/',
            StorageName: 'plandoc',
            CurrentLanguage: 'pt-BR'
        };

        return service;
    }

}());