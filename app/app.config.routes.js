﻿(function () {
    'use strict';
    angular
        .module('app')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('login');

        $stateProvider


            .state('comprarEcardPagamentoDebitoFinalizada',
            {
                url: '/comprarEcardPagamentoDebitoFinalizada',
                templateUrl: "app/modules/comprarEcardPagamentoDebitoFinalizada/comprarEcardPagamentoDebitoFinalizada.html",
                controller: "ComprarEcardPagamentoDebitoFinalizadaController",
                data: { pageTitle: 'comprarEcardPagamentoDebitoFinalizada' },
                resolve: {

                }
            })

            .state('comprarEcardPagamentoDebito',
            {
                url: '/comprarEcardPagamentoDebito',
                templateUrl: "app/modules/comprarEcardPagamentoDebito/comprarEcardPagamentoDebito.html",
                controller: "ComprarEcardPagamentoDebitoController",
                data: { pageTitle: 'comprarEcardPagamentoDebito' },
                resolve: {

                }
            })

            .state('quantoSalvei',
            {
                url: '/quantoSalvei',
                templateUrl: "app/modules/quantoSalvei/quantoSalvei.html",
                controller: "QuantoSalveiController",
                data: { pageTitle: 'quantoSalvei' },
                resolve: {

                }
            })

            .state('convidarAmigos',
            {
                url: '/convidarAmigos',
                templateUrl: "app/modules/convidarAmigos/convidarAmigos.html",
                controller: "ConvidarAmigosController",
                data: { pageTitle: 'convidarAmigos' },
                resolve: {

                }
            })

            .state('comprarEcardPagamentoCreditoFinalizada',
            {
                url: '/comprarEcardPagamentoCreditoFinalizada',
                templateUrl: "app/modules/comprarEcardPagamentoCreditoFinalizada/comprarEcardPagamentoCreditoFinalizada.html",
                controller: "ComprarEcardPagamentoCreditoFinalizadaController",
                data: { pageTitle: 'comprarEcardPagamentoCreditoFinalizada' },
                resolve: {

                }
            })

            .state('comprarEcardPagamentoCredito',
            {
                url: '/comprarEcardPagamentoCredito',
                templateUrl: "app/modules/comprarEcardPagamentoCredito/comprarEcardPagamentoCredito.html",
                controller: "ComprarEcardPagamentoCreditoController",
                data: { pageTitle: 'comprarEcardPagamentoCredito' },
                resolve: {

                }
            })

            .state('comprarEcardPagamento',
            {
                url: '/comprarEcardPagamento',
                templateUrl: "app/modules/comprarEcardPagamento/comprarEcardPagamento.html",
                controller: "ComprarEcardPagamentoController",
                data: { pageTitle: 'ComprarEcardPagamento' },
                resolve: {

                }
            })

            .state('comprarEcardEndereco',
            {
                url: '/comprarEcardEndereco',
                templateUrl: "app/modules/comprarEcardEndereco/comprarEcardEndereco.html",
                controller: "ComprarEcardEnderecoController",
                data: { pageTitle: 'ComprarEcardEnderecoController' },
                resolve: {

                }
            })

            .state('comprarEcardDados',
            {
                url: '/comprarEcardDados',
                templateUrl: "app/modules/comprarEcardDados/comprarEcardDados.html",
                controller: "ComprarEcardDadosController",
                data: { pageTitle: 'Comprar E-card' },
                resolve: {

                }
            })

            .state('faq',
            {
                url: '/faq',
                templateUrl: "app/modules/faq/faq.html",
                controller: "FaqController",
                data: { pageTitle: 'FAQ' },
                resolve: {

                }
            })

            .state('cadastroConfirmaEmail',
            {
                url: '/cadastroConfirmaEmail',
                templateUrl: "app/modules/cadastroConfirmaEmail/cadastroConfirmaEmail.html",
                controller: "CadastroConfirmaEmailController",
                data: { pageTitle: 'cadastroConfirmaEmail' },
                resolve: {

                }
            })

            .state('cadastro1',
            {
                url: '/cadastro1',
                templateUrl: "app/modules/cadastro1/cadastro1.html",
                controller: "Cadastro1Controller",
                data: { pageTitle: 'Cadastro' },
                resolve: {

                }
            })

            .state('login',
            {
                url: '/login',
                templateUrl: "app/modules/login/login.html",
                controller: "LoginController",
                data: { pageTitle: 'Login' },
                resolve: {

                }
            })

            .state('home',
            {
                url: '/home',
                templateUrl: "app/modules/home/home.html",
                controller: "HomeController",
                data: { pageTitle: 'Home' },
                resolve: {

                }
            })

            .state('comoFunciona',
            {
                url: '/comoFunciona',
                templateUrl: "app/modules/comoFunciona/comoFunciona.html",
                controller: "ComoFuncionaController",
                data: { pageTitle: 'Como Funciona' },
                resolve: {

                }
            })

            .state('comprarEcard',
            {
                url: '/comprarEcard',
                templateUrl: "app/modules/comprarEcard/comprarEcard.html",
                controller: "ComprarEcardController",
                data: { pageTitle: 'Comprar Ecard' },
                resolve: {

                }
            })

            .state('cadastro',
            {
                url: '/cadastro',
                templateUrl: "app/modules/cadastro/cadastro.html",
                controller: "CadastroController",
                data: { pageTitle: 'Cadastro' },
                resolve: {

                }
            })

    }

}());
