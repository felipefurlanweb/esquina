<?php 
	header('Access-Control-Allow-Origin: *');
	include_once 'banco.php';

	$id = $_POST["id"];
	//$id = $_GET["id"];
	//var_dump($post);

	switch ($id) {

		case 'getAllOrquideas':
			getAllOrquideas();
			break;

		case 'getOrquideaById':
			getOrquideaById();
			break;

		case 'getAllVideos':
			getAllVideos();
			break;

		case 'getAllCategorias':
			getAllCategorias();
			break;

		case 'getAllCoordenadores':
			getAllCoordenadores();
			break;

		case 'empresaAdd':
			empresaAdd();
			break;

		case 'getAllEmpresas':
			getAllEmpresas();
			break;

		case 'plantaoAdd':
			plantaoAdd();
			break;

		case 'compromissoAdd':
			compromissoAdd();
			break;

		case 'getAllPlantoes':
			getAllPlantoes();
			break;

		case 'getAllCompromissos':
			getAllCompromissos();
			break;

		case 'plantaoSearch':
			plantaoSearch();
			break;

		case 'plantaoSearchPagoAberto':
			plantaoSearchPagoAberto();
			break;

		case 'plantaoSearchValorTotal':
			plantaoSearchValorTotal();
			break;

		case 'getPlantaoById':
			getPlantaoById();
			break;

		case 'plantaoEdit':
			plantaoEdit();
			break;

		case 'relatorioSearch':
			relatorioSearch();
			break;
		case 'relatorioSearchPago':
			relatorioSearchPago();
			break;

		case 'financeiroPlantaoAno':
			financeiroPlantaoAno();
			break;

		case 'trocaPlantao':
			trocaPlantao();
			break;	

		case 'getEmpresaByMes':
			getEmpresaByMes();
			break;		

		case 'getAllPlantoesAbertos':
			getAllPlantoesAbertos();
			break;				

		case 'getAllPlantoesPagos':
			getAllPlantoesPagos();
			break;				

		default:
			# code...
			break;
	}
 
 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllPlantoesPagos(){
		$query = "SELECT *, e.nome as nomeEmpresa FROM plantao p 
			INNER JOIN empresas e ON p.idEmpresa = e.id
			WHERE p.aberto = 2";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {

			$valorPagamento = $r["valorPagamento"];
			$valorPagamento = str_replace(".", ",", $valorPagamento);
			$r["valorPagamento"] = "R$ ".$valorPagamento;
			

			$data = $r["data"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$r["data"] = $dia.'/'.$mes.'/'.$ano;

		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllPlantoesAbertos(){
		$query = "SELECT *, e.nome as nomeEmpresa FROM plantao p 
			INNER JOIN empresas e ON p.idEmpresa = e.id
			WHERE p.aberto = 1";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {

			$valorPagamento = $r["valorPagamento"];
			$valorPagamento = str_replace(".", ",", $valorPagamento);
			$r["valorPagamento"] = "R$ ".$valorPagamento;
			

			$data = $r["data"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$r["data"] = $dia.'/'.$mes.'/'.$ano;

		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getEmpresaByMes(){
		$mes = $_POST["mes"];
		$query = "SELECT p.data, p.valorPagamento, e.nome FROM plantao p 
			INNER JOIN empresas e ON p.idEmpresa = e.id
			WHERE (EXTRACT( MONTH FROM p.data ) = $mes)";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {

			$valorPagamento = $r["valorPagamento"];
			$valorPagamento = str_replace(".", ",", $valorPagamento);
			$r["valorPagamento"] = "R$ ".$valorPagamento;
			

			$data = $r["data"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$r["data"] = $dia.'/'.$mes.'/'.$ano;

		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function trocaPlantao(){
		if (isset($_POST["model"])) {
			$model = $_POST["model"];
			$idPlantao = $model["idPlantao"];
			$idEmpresa = $model["idEmpresa"];
			$query = "UPDATE plantao SET idEmpresa = $idEmpresa WHERE id = $idPlantao";
			$query = mysql_query($query);
		}
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function financeiroPlantaoAno(){
		$query = "SELECT SUM(valorPagamento) as total, COUNT(data) as qtd, data FROM plantao GROUP BY (EXTRACT( YEAR FROM data ))";
		$query = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($query)) {
			$total = $r["total"];
			$total = str_replace(".", ",", $total);
			$total = "R$ ".$total;
			$r["total"] = $total;

			$data = $r["data"];
			$data = substr($data, 0, 4);
			$r["data"] = $data;

		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function relatorioSearch(){
		
		$ano = 0;
		$idEmpresa = "";
		if (isset($_POST["model"])) {
			$model = $_POST["model"];			
			if (isset($model["ano"])) {
				$ano = $model["ano"];
			}
			if (isset($model["idEmpresa"])) {
				$idEmpresa = $model["idEmpresa"];
			}
		}
		
		$query = "SELECT *, e.nome as nomeEmpresa FROM empresas e
					INNER JOIN plantao p ON e.id = p.idEmpresa
					WHERE (p.aberto = 1) ";
		if ($ano > 0) {
			$query .= "AND (EXTRACT( YEAR FROM p.data ) = $ano)";
		}
		if ($idEmpresa != "") {
			$query .= "AND (p.idEmpresa = $idEmpresa)";
		}
		$sth = mysql_query($query);

		$rows = array();
		$meses = array();

		$jan = 0; $valorjan = 0.00; $totaljan = 0.00;
		$fev = 0; $valorfev = 0.00; $totalfev = 0.00;
		$mar = 0; $valormar = 0.00; $totalmar = 0.00;
		$abr = 0; $valorabr = 0.00; $totalabr = 0.00;
		$mai = 0; $valormai = 0.00; $totalmai = 0.00;
		$jun = 0; $valorjun = 0.00; $totaljun = 0.00;
		$jul = 0; $valorjul = 0.00; $totaljul = 0.00;
		$ago = 0; $valorago = 0.00; $totalago = 0.00;
		$set = 0; $valorset = 0.00; $totalset = 0.00;
		$out = 0; $valorout = 0.00; $totalout = 0.00;
		$nov = 0; $valornov = 0.00; $totalnov = 0.00;
		$dez = 0; $valordez = 0.00; $totaldez = 0.00;

		while($r = mysql_fetch_assoc($sth)) {
			$data = $r["data"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$data = $dia.'/'.$mes.'/'.$ano.' '.$hora.':'.$minuto;
			$r["data"] = $data;


			if ($mes == "01") { $jan ++; $mes = "jan"; $meses["jan"] = $jan; $valorjan = $valorjan + $r["valorPagamento"]; $meses["valorjan"] = $valorjan; }
			if ($mes == "02") { $fev ++; $mes = "fev"; $meses["fev"] = $fev; $valorfev = $valorfev + $r["valorPagamento"]; $meses["valorfev"] = $valorfev; }
			if ($mes == "03") { $mar ++; $mes = "mar"; $meses["mar"] = $mar; $valormar = $valormar + $r["valorPagamento"]; $meses["valormar"] = $valormar; }
			if ($mes == "04") { $abr ++; $mes = "abr"; $meses["abr"] = $abr; $valorabr = $valorabr + $r["valorPagamento"]; $meses["valorabr"] = $valorabr; }
			if ($mes == "05") { $mai ++; $mes = "mai"; $meses["mai"] = $mai; $valormai = $valormai + $r["valorPagamento"]; $meses["valormai"] = $valormai; }
			if ($mes == "06") { $jun ++; $mes = "jun"; $meses["jun"] = $jun; $valorjun = $valorjun + $r["valorPagamento"]; $meses["valorjun"] = $valorjun; }
			if ($mes == "07") { $jul ++; $mes = "jul"; $meses["jul"] = $jul; $valorjul = $valorjul + $r["valorPagamento"]; $meses["valorjul"] = $valorjul; }
			if ($mes == "08") { $ago ++; $mes = "ago"; $meses["ago"] = $ago; $valorago = $valorago + $r["valorPagamento"]; $meses["valorago"] = $valorago; }
			if ($mes == "09") { $set ++; $mes = "set"; $meses["set"] = $set; $valorset = $valorset + $r["valorPagamento"]; $meses["valorset"] = $valorset; }
			if ($mes == "10") { $out ++; $mes = "out"; $meses["out"] = $out; $valorout = $valorout + $r["valorPagamento"]; $meses["valorout"] = $valorout; }
			if ($mes == "11") { $nov ++; $mes = "nov"; $meses["nov"] = $nov; $valornov = $valornov + $r["valorPagamento"]; $meses["valornov"] = $valornov; }
			if ($mes == "12") { $dez ++; $mes = "dez"; $meses["dez"] = $dez; $valordez = $valordez + $r["valorPagamento"]; $meses["valordez"] = $valordez; }
						$rows[] = $r;
		}		
		echo json_encode($meses);
	}

	function relatorioSearchPago(){
		
		$ano = 0;
		$idEmpresa = "";
		if (isset($_POST["model"])) {
			$model = $_POST["model"];			
			if (isset($model["ano"])) {
				$ano = $model["ano"];
			}
			if (isset($model["idEmpresa"])) {
				$idEmpresa = $model["idEmpresa"];
			}
		}
		
		$query = "SELECT *, e.nome as nomeEmpresa FROM empresas e
					INNER JOIN plantao p ON e.id = p.idEmpresa
					WHERE (p.aberto = 2) ";
		if ($ano > 0) {
			$query .= "AND (EXTRACT( YEAR FROM p.data ) = $ano)";
		}
		if ($idEmpresa != "") {
			$query .= "AND (p.idEmpresa = $idEmpresa)";
		}
		$sth = mysql_query($query);

		$rows = array();
		$meses = array();

		$jan = 0; $valorjan = 0.00; $totaljan = 0.00;
		$fev = 0; $valorfev = 0.00; $totalfev = 0.00;
		$mar = 0; $valormar = 0.00; $totalmar = 0.00;
		$abr = 0; $valorabr = 0.00; $totalabr = 0.00;
		$mai = 0; $valormai = 0.00; $totalmai = 0.00;
		$jun = 0; $valorjun = 0.00; $totaljun = 0.00;
		$jul = 0; $valorjul = 0.00; $totaljul = 0.00;
		$ago = 0; $valorago = 0.00; $totalago = 0.00;
		$set = 0; $valorset = 0.00; $totalset = 0.00;
		$out = 0; $valorout = 0.00; $totalout = 0.00;
		$nov = 0; $valornov = 0.00; $totalnov = 0.00;
		$dez = 0; $valordez = 0.00; $totaldez = 0.00;

		while($r = mysql_fetch_assoc($sth)) {
			$data = $r["data"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$data = $dia.'/'.$mes.'/'.$ano.' '.$hora.':'.$minuto;
			$r["data"] = $data;


			if ($mes == "01") { $jan ++; $mes = "jan"; $meses["jan"] = $jan; $valorjan = $valorjan + $r["valorPagamento"]; $meses["valorjan"] = $valorjan; }
			if ($mes == "02") { $fev ++; $mes = "fev"; $meses["fev"] = $fev; $valorfev = $valorfev + $r["valorPagamento"]; $meses["valorfev"] = $valorfev; }
			if ($mes == "03") { $mar ++; $mes = "mar"; $meses["mar"] = $mar; $valormar = $valormar + $r["valorPagamento"]; $meses["valormar"] = $valormar; }
			if ($mes == "04") { $abr ++; $mes = "abr"; $meses["abr"] = $abr; $valorabr = $valorabr + $r["valorPagamento"]; $meses["valorabr"] = $valorabr; }
			if ($mes == "05") { $mai ++; $mes = "mai"; $meses["mai"] = $mai; $valormai = $valormai + $r["valorPagamento"]; $meses["valormai"] = $valormai; }
			if ($mes == "06") { $jun ++; $mes = "jun"; $meses["jun"] = $jun; $valorjun = $valorjun + $r["valorPagamento"]; $meses["valorjun"] = $valorjun; }
			if ($mes == "07") { $jul ++; $mes = "jul"; $meses["jul"] = $jul; $valorjul = $valorjul + $r["valorPagamento"]; $meses["valorjul"] = $valorjul; }
			if ($mes == "08") { $ago ++; $mes = "ago"; $meses["ago"] = $ago; $valorago = $valorago + $r["valorPagamento"]; $meses["valorago"] = $valorago; }
			if ($mes == "09") { $set ++; $mes = "set"; $meses["set"] = $set; $valorset = $valorset + $r["valorPagamento"]; $meses["valorset"] = $valorset; }
			if ($mes == "10") { $out ++; $mes = "out"; $meses["out"] = $out; $valorout = $valorout + $r["valorPagamento"]; $meses["valorout"] = $valorout; }
			if ($mes == "11") { $nov ++; $mes = "nov"; $meses["nov"] = $nov; $valornov = $valornov + $r["valorPagamento"]; $meses["valornov"] = $valornov; }
			if ($mes == "12") { $dez ++; $mes = "dez"; $meses["dez"] = $dez; $valordez = $valordez + $r["valorPagamento"]; $meses["valordez"] = $valordez; }
						$rows[] = $r;
		}		

		echo json_encode($meses);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

 	function plantaoEdit(){
		$model = $_POST["model"];
		$id = $model["id"];
		$aberto = $model["aberto"];

		$idEmpresa = $model["idEmpresa"];
		$data = $model["data"];
		$hora = $model["hora"];
		$fixo = $model["fixo"];
		$turno = $model["turno"];
		$valorPagamento = $model["valorPagamento"];
		$valorPagamento = str_replace("R$ ", "", $valorPagamento);
		$valorPagamento = str_replace(".", "", $valorPagamento);
		$valorPagamento = str_replace(",", ".", $valorPagamento);
		$formaPagamento = $model["formaPagamento"];

		$ano = substr($data, 11, 4);
		$mes = substr($data, 4, 3);
		if ($mes == "Jan") { $mes = "1"; }
		if ($mes == "Feb") { $mes = "2"; }
		if ($mes == "Mar") { $mes = "3"; }
		if ($mes == "Apr") { $mes = "4"; }
		if ($mes == "May") { $mes = "5"; }
		if ($mes == "Jun") { $mes = "6"; }
		if ($mes == "Jul") { $mes = "7"; }
		if ($mes == "Aug") { $mes = "8"; }
		if ($mes == "Sep") { $mes = "9"; }
		if ($mes == "Oct") { $mes = "10"; }
		if ($mes == "Nov") { $mes = "11"; }
		if ($mes == "Dec") { $mes = "12"; }
		
		$dia = substr($data, 8, 2);
		$time = strtotime(''.$mes.'/'.$dia.'/'.$ano.' '.$hora);
		$data = date('Y-m-d H:m',$time);
	
		if ($formaPagamento == "2") {
			$dataPrazo = $model["dataPrazo"];
		}else{
			$dataPrazo = "";
		}

		if ($formaPagamento == "3") {
			$semanas = $model["semanas"];
		}else{
			$semanas = "";
		}
		
		$query = "UPDATE plantao 
		SET
		idEmpresa = '%s',
		data = '%s',
		fixo = '%s',
		turno = '%s',
		valorPagamento = '%s',
		formaPagamento = '%s',
		dataPrazo = '%s',
		semanas = '%s',
		aberto = '%s' 
		WHERE id = $id";
		$query = sprintf($query,
		addslashes($idEmpresa),
		addslashes($data),
		addslashes($fixo),
		addslashes($turno),
		addslashes($valorPagamento),
		addslashes($formaPagamento),
		addslashes($dataPrazo),
		addslashes($semanas),
		addslashes($aberto)
		);
		$query = mysql_query($query);
		if ($query) {
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
 	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getPlantaoById(){
		$model = $_POST["model"];
		$id = $model["id"];
		$aberto = $model["aberto"];
		$query = "SELECT * FROM plantao WHERE idEmpresa = $id AND aberto = $aberto";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {

			$idEmpresa = $r["idEmpresa"];
			$query2 = "SELECT * FROM empresas WHERE id = $idEmpresa";
			$query2 = mysql_query($query2);
			$res2 = mysql_fetch_assoc($query2);
			$nomeEmpresa = $res2["nome"];

			$data = $r["data"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$r["data"] = $dia.'/'.$mes.'/'.$ano;
			$r["hora"] = $hora.':'.$minuto;

			$dataPrazo = $r["dataPrazo"];
			$dia = substr($dataPrazo, 8, 2);
			$mes = substr($dataPrazo, 5, 2);
			$ano = substr($dataPrazo, 0, 4);
			$r["dataPrazo"] = $dia.'/'.$mes.'/'.$ano;

			$valorPagamento = $r["valorPagamento"];
			$valorPagamento = str_replace(".", ",", $valorPagamento);
			$r["valorPagamento"] = $valorPagamento;

		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function plantaoSearchValorTotal(){
		$whereMes = "";
		$whereAno = "";
		if (isset($_POST["model"])){
			$model = $_POST["model"];
			if (isset($model["mes"])) {
				$mes = $model["mes"];
				if ($mes != "") {
					$whereMes = "WHERE (EXTRACT( MONTH FROM `data` ) = $mes)";
				}
			}
			if (isset($model["ano"])) {
				$ano = $model["ano"];
				if (isset($model["mes"])) {
					if ($mes != "") {
						if ($ano != "") {
							$whereAno = "AND (EXTRACT( YEAR FROM `data` ) = $ano)";
						}
					}
				}else{
					$whereAno = "WHERE (EXTRACT( YEAR FROM `data` ) = $ano)";
				}
			}
		}
		$query = "SELECT *, SUM(p.valorPagamento) as valorTotal, e.nome as nomeEmpresa
						FROM plantao p
						INNER JOIN empresas e ON p.idEmpresa = e.id ";
		if ($whereMes != "") {
			$query = $query.$whereMes;
		}
		if ($whereAno != "") {
			$query = $query.$whereAno;
		}
						
		$sth = mysql_query($query);
		$rows = array();
		if (mysql_num_rows($sth) > 0) {
			while($r = mysql_fetch_assoc($sth)) {

				$valorTotal = $r["valorTotal"];
				$valorTotal = str_replace(".", ",", $valorTotal);
				$r["valorTotal"] = $valorTotal;

			    $rows[] = $r;
			}
		}

		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function plantaoSearchPagoAberto(){
		$whereStatus = "";
		$whereMes = "";
		$whereAno = "";
		if (isset($_POST["model"])){
			$model = $_POST["model"];
			if (isset($model["status"])) {
				$aberto = $model["status"];
				if ($aberto != "") {
					$whereStatus = "WHERE (p.aberto = $aberto)";
				}				
			}else{
				$whereStatus = "WHERE (p.aberto = 1 OR p.aberto = 2)";
			}
			if (isset($model["mes"])) {
				$mes = $model["mes"];
				if ($mes != "") {
					$whereMes = "AND (EXTRACT( MONTH FROM `data` ) = $mes)";
				}
			}
			if (isset($model["ano"])) {
				$ano = $model["ano"];
				if ($ano != "") {
					$whereAno = "AND (EXTRACT( YEAR FROM `data` ) = $ano)";
				}
			}
		}
		$query = "SELECT *, e.nome as nomeEmpresa
						FROM plantao p
						INNER JOIN empresas e ON p.idEmpresa = e.id 
						".$whereStatus."
						".$whereMes."
						".$whereAno;

		$sth = mysql_query($query);
		$rows = array();
		if (mysql_num_rows($sth) > 0) {
			while($r = mysql_fetch_assoc($sth)) {
				$data = $r["data"];
				$dia = substr($data, 8, 2);
				$mes = substr($data, 5, 2);
				$ano = substr($data, 0, 4);
				$hora = substr($data, 11, 2);
				$minuto = substr($data, 14, 2);
				$data = $dia.'/'.$mes.'/'.$ano.' '.$hora.':'.$minuto;
				$r["data"] = $data;
			    $rows[] = $r;
			}
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function plantaoSearch(){
		$where = "";
		if (isset($_POST["model"])){
			$model = $_POST["model"];

			if (isset($model["idEmpresa"])) {
				$idEmpresa = $model["idEmpresa"];
				if ($idEmpresa != "") {
					$where .= "AND (e.id = $idEmpresa) ";
				}
			}

			if (isset($model["fixo"])) {
				$fixo = $model["fixo"];
				if ($fixo != "") {
					$where .= "AND (p.fixo = $fixo) ";
				}
			}

			if (isset($model["turno"])) {
				$turno = $model["turno"];
				if ($turno != "") {
					$where .= "AND (p.turno = $turno) ";
				}
			}

			if (isset($model["valorPagamento"])) {
				$valorPagamento = $model["valorPagamento"];
				$valorPagamento = str_replace("R$ ", "", $valorPagamento);
				$valorPagamento = str_replace(".", "", $valorPagamento);
				$valorPagamento = str_replace(",", ".", $valorPagamento);
				if ($valorPagamento != "") {
					$where .= "AND (p.valorPagamento = $valorPagamento) ";
				}
			}

			if (isset($model["formaPagamento"])) {
				$formaPagamento = $model["formaPagamento"];
				if ($formaPagamento != "") {
					$where .= "AND (p.formaPagamento = $formaPagamento) ";
				}
			}

			if (isset($model["semanas"])) {
				$semanas = $model["semanas"];
				if ($semanas != "") {
					$where .= "AND (p.semanas = $semanas) ";
				}
			}

			if (isset($model["dataPrazo"])) {
				$data = $model["dataPrazo"];
				$ano = substr($data, 11, 4);
				$mes = substr($data, 4, 3);
				if ($mes == "Jan") { $mes = "1"; }
				if ($mes == "Feb") { $mes = "2"; }
				if ($mes == "Mar") { $mes = "3"; }
				if ($mes == "Apr") { $mes = "4"; }
				if ($mes == "May") { $mes = "5"; }
				if ($mes == "Jun") { $mes = "6"; }
				if ($mes == "Jul") { $mes = "7"; }
				if ($mes == "Aug") { $mes = "8"; }
				if ($mes == "Sep") { $mes = "9"; }
				if ($mes == "Oct") { $mes = "10"; }
				if ($mes == "Nov") { $mes = "11"; }
				if ($mes == "Dec") { $mes = "12"; }
				
				$dia = substr($data, 8, 2);
				$time = strtotime(''.$mes.'/'.$dia.'/'.$ano);
				$data = date('Y-m-d',$time);
				if ($data != "" && $data != "1970-01-01") {
					$where .= "AND (p.dataPrazo = $data) ";
				}
			}

			if (isset($model["aberto"])) {
				$aberto = $model["aberto"];
				if ($aberto != "") {
					$where .= "AND (p.aberto = $aberto) ";
				}
			}

		}
		$query = "SELECT *, e.nome as nomeEmpresa
						FROM plantao p
						INNER JOIN empresas e ON p.idEmpresa = e.id
						WHERE (aberto = 1 OR aberto = 2) ";
		$query .= $where;
		$sth = mysql_query($query);
		$rows = array();
		if (mysql_num_rows($sth) > 0) {
			while($r = mysql_fetch_assoc($sth)) {
				$data = $r["data"];
				$dia = substr($data, 8, 2);
				$mes = substr($data, 5, 2);
				$ano = substr($data, 0, 4);
				$hora = substr($data, 11, 2);
				$minuto = substr($data, 14, 2);
				$data = $dia.'/'.$mes.'/'.$ano.' '.$hora.':'.$minuto;
				$r["data"] = $data;

				$dataPrazo = $r["dataPrazo"];
				$dia = substr($dataPrazo, 8, 2);
				$mes = substr($dataPrazo, 5, 2);
				$ano = substr($dataPrazo, 0, 4);
				$dataPrazo = $dia.'/'.$mes.'/'.$ano;
				$r["dataPrazo"] = $dataPrazo;

			    $rows[] = $r;
			}
		}

		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllCompromissos(){
		$query = "SELECT * FROM compromissos ORDER BY dataInicio DESC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
			$data = $r["dataInicio"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$data = $dia.'/'.$mes.'/'.$ano.' '.$hora.':'.$minuto;
			$r["dataInicio"] = $data;

			$data = $r["dataFim"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$hora = substr($data, 11, 2);
			$minuto = substr($data, 14, 2);
			$data = $dia.'/'.$mes.'/'.$ano.' '.$hora.':'.$minuto;
			$r["dataFim"] = $data;

			$descricao = $r["descricao"];
			$descricao = substr($descricao, 0, 10). "...";
			$r["descricao"] = $descricao;


		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllPlantoes(){
		$query = "SELECT *, p.id as idPlantao, e.nome as nomeEmpresa FROM plantao p
		INNER JOIN empresas e ON p.idEmpresa = e.id
		ORDER BY p.dataCadastro DESC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
			$data = $r["data"];
			$dia = substr($data, 8, 2);
			$mes = substr($data, 5, 2);
			$ano = substr($data, 0, 4);
			$data = $dia.'/'.$mes.'/'.$ano;
			$r["data"] = $data;

			$valorPagamento = $r["valorPagamento"];
			$valorPagamento = str_replace(".", ",", $valorPagamento);
			$r["valorPagamento"] = $valorPagamento;

		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

 	function compromissoAdd(){
		$model = $_POST["model"];
		$dataInicio = $model["dataInicio"];
		$horaInicio = $model["horaInicio"];
		// $dataInicio = "15/08/1991";
		// $horaInicio = "10:30";
		$dia = substr($dataInicio, 0, 2);
		$mes = substr($dataInicio, 3, 2);
		$ano = substr($dataInicio, 6, 4);
		$time = strtotime(''.$mes.'/'.$dia.'/'.$ano.' '.$horaInicio);
		$dataInicio = date('Y-m-d H:m',$time);

		$dataFim = $model["dataFim"];
		$horaFim = $model["horaFim"];
		// $dataFim = "15/09/1991";
		// $horaFim = "10:30";
		$dia = substr($dataFim, 0, 2);
		$mes = substr($dataFim, 3, 2);
		$ano = substr($dataFim, 6, 4);
		$time = strtotime(''.$mes.'/'.$dia.'/'.$ano.' '.$horaFim);
		$dataFim = date('Y-m-d H:m',$time);

		$descricao = "";
		if (isset($model["descricao"])) {
			$descricao = $model["descricao"];
		}

		$query = "INSERT INTO compromissos (dataInicio, dataFim, descricao) VALUES ('$dataInicio', '$dataFim', '%s')";
		$query = sprintf($query, addslashes($descricao));
		$query = mysql_query($query);
		if ($query) {
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
 	}

 	/////////////////////////////////////////////////////////////////////////////////////

 	function plantaoAdd(){
 		
		$model = $_POST["model"];

		$idEmpresa = $model["idEmpresa"];
		$data = $model["data"];
		$hora = $model["hora"];
		$fixo = $model["fixo"];
		$turno = $model["turno"];
		$valorPagamento = $model["valorPagamento"];
		$valorPagamento = str_replace("R$ ", "", $valorPagamento);
		$valorPagamento = str_replace(".", "", $valorPagamento);
		$valorPagamento = str_replace(",", ".", $valorPagamento);
		$formaPagamento = $model["formaPagamento"];

		$ano = substr($data, 11, 4);
		$mes = substr($data, 4, 3);
		if ($mes == "Jan") { $mes = "1"; }
		if ($mes == "Feb") { $mes = "2"; }
		if ($mes == "Mar") { $mes = "3"; }
		if ($mes == "Apr") { $mes = "4"; }
		if ($mes == "May") { $mes = "5"; }
		if ($mes == "Jun") { $mes = "6"; }
		if ($mes == "Jul") { $mes = "7"; }
		if ($mes == "Aug") { $mes = "8"; }
		if ($mes == "Sep") { $mes = "9"; }
		if ($mes == "Oct") { $mes = "10"; }
		if ($mes == "Nov") { $mes = "11"; }
		if ($mes == "Dec") { $mes = "12"; }
		
		$dia = substr($data, 8, 2);
		$time = strtotime(''.$mes.'/'.$dia.'/'.$ano.' '.$hora);
		$data = date('Y-m-d H:m',$time);
	
		if ($formaPagamento == "2") {
			$dataPrazo = $model["dataPrazo"];
			$ano = substr($dataPrazo, 11, 4);
			$mes = substr($dataPrazo, 4, 3);
			if ($mes == "Jan") { $mes = "1"; }
			if ($mes == "Feb") { $mes = "2"; }
			if ($mes == "Mar") { $mes = "3"; }
			if ($mes == "Apr") { $mes = "4"; }
			if ($mes == "May") { $mes = "5"; }
			if ($mes == "Jun") { $mes = "6"; }
			if ($mes == "Jul") { $mes = "7"; }
			if ($mes == "Aug") { $mes = "8"; }
			if ($mes == "Sep") { $mes = "9"; }
			if ($mes == "Oct") { $mes = "10"; }
			if ($mes == "Nov") { $mes = "11"; }
			if ($mes == "Dec") { $mes = "12"; }
			
			$dia = substr($dataPrazo, 8, 2);
			$time = strtotime(''.$mes.'/'.$dia.'/'.$ano.' '.$hora);
			$dataPrazo = date('Y-m-d',$time);
		}else{
			$dataPrazo = "";
		}

		if ($formaPagamento == "3") {
			$semanas = $model["semanas"];
		}else{
			$semanas = "";
		}

		$query = "INSERT INTO plantao 
		(
		idEmpresa,
		data,
		fixo,
		turno,
		valorPagamento,
		formaPagamento,
		dataPrazo,
		semanas
		) VALUES 
		(
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s',
		'%s'
		)
		";
		$query = sprintf($query,
		addslashes($idEmpresa),
		addslashes($data),
		addslashes($fixo),
		addslashes($turno),
		addslashes($valorPagamento),
		addslashes($formaPagamento),
		addslashes($dataPrazo),
		addslashes($semanas)
		);
		$query = mysql_query($query);
		if ($query) {
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
 	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllEmpresas(){
		$query = "SELECT * FROM empresas ORDER BY nome ASC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllCoordenadores(){
		$query = "SELECT * FROM coordenadores ORDER BY nome ASC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllCategorias(){
		$query = "SELECT * FROM categorias ORDER BY nome ASC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

 	/////////////////////////////////////////////////////////////////////////////////////

 	function empresaAdd(){
		$model = $_POST["model"];
		$nome = $model["nome"];
		$categoria = $model["categoria"];
		$endereco = $model["endereco"];
		$coordenador = $model["coordenador"];
		$contato = $model["contato"];
		$cor = $model["cor"];
		$query = "INSERT INTO empresas (nome, categoria, endereco, coordenador, contato, cor) VALUES ";
		$query .= "('%s', '%s', '%s', '%s', '%s', '%s')";
		$query = sprintf($query, addslashes($nome), addslashes($categoria), addslashes($endereco), 
			addslashes($coordenador), addslashes($contato), addslashes($cor));
		$query = mysql_query($query);
		if ($query) {
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
 	}

 	/////////////////////////////////////////////////////////////////////////////////////

	function getAllOrquideas(){
		$query = "SELECT * FROM orquideas ORDER BY nome ASC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

	/////////////////////////////////////////////////////////////////////////////////////

	function getAllVideos(){
		$query = "SELECT * FROM videos ORDER BY id DESC";
		$sth = mysql_query($query);
		$rows = array();
		while($r = mysql_fetch_assoc($sth)) {
			$url = $r["url"];
			$url = str_replace('https://www.youtube.com/watch?v=', "", $url);
			$url = str_replace('http://www.youtube.com/watch?v=', "", $url);
			$r["url"] = $url;
		    $rows[] = $r;
		}
		echo json_encode($rows);
	}

	/////////////////////////////////////////////////////////////////////////////////////


?>